$('document').ready(JplaceHolder);
function JplaceHolder() {
    var ajaxSetting = {
        url: 'https://jsonplaceholder.typicode.com/users',
        complete: function (jqxhr, status) {
            if (status === "success") {

                var users = jqxhr.responseJSON;

                for (var i = 0; i < users.length; i++) {
                    var html = [
                        {
                            'NAme': 'users[i].name',
                            'Adress': 'users[i].address.suite',
                            'Street': 'users[i].address.street',
                            'City': 'users[i].address.city',
                            'Zip COde': 'users[i].address.zipcode'
                        }
                    ];
                    $("#grid").igGrid({
                        dataSource: users //JSON Array defined above
                    })
                }
            }
        }
    }
    $.ajax(ajaxSetting);
}
/*

+
'<div class="card-action"> ' +
'<a id="' + users[i].id + '" class="showPosts" ayoub="' + users[i].id + '">Voir les posts</a>' +
'</div>'+
'</div>';

$('body').append($(html))
}
// tous les buttons sont presents dans le body
// je peu ajouter l'evenement
$('.showPosts').click(function (event) {
event.preventDefault();
$.ajax({
url: 'https://jsonplaceholder.typicode.com/posts?userId=' + event.target.id,
complete: function (jqxhr, status) {
    var userPosts = jqxhr.responseJSON;

    var html = '<ul class="collection">';
    for (var i = 0; i < userPosts.length; i++) {
        html += '<li class="collection-item">' + userPosts[i].title + '     '+ '<div style="text-align: right" >' +
            '<a id="'+userPosts[i].id+'" class="btn-floating waves-effect waves-light red btn showComment">+</a>' +
            '</div>'
            + '</li>'
    }

    html += '</ul>';
    ($(html)).insertAfter($(event.target).parent().parent())

    $('.showComment').click(function (event) {
        event.preventDefault();
        $.ajax({
            url: 'https://jsonplaceholder.typicode.com/comments?postId=' + event.target.id,
            complete: function (jqxhr, status) {
                var comment = jqxhr.responseJSON;

                var htmlCom = '<ul class="collection">';
                for (var j = 0; j < comment.length; j++) {
                    htmlCom += '<li class="collection-item">' + comment[j].body + '</li>'
                }
                htmlCom += '</ul>';
                ($(htmlCom)).insertAfter($(event.target).parent())
            }
        })
    });
}
})
});

}
}
}

$.ajax(ajaxSetting);
}

/*

$('document').ready(JplaceHolder);

function JplaceHolder() {
var jsondata = {
url: 'https://jsonplaceholder.typicode.com/users',
var users = jqxhr.responseJSON;
$.each(jsondata.response, function(i, d) {
var row='<tr>';
$.each(d, function(j, e) {
row+='<td>'+e+'</td>';
});
row+='</tr>';
$('#table tbody').append(row);
});
*/