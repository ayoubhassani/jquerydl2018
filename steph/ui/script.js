$('document').ready(Ui);

function Ui(){
    $(document).tooltip();
    $('body > div:nth-child(2)').progressbar({
        value:50
    });
    $('body > div:nth-child(4)').slider({
        change:function(event, ui){
            $('body > div:nth-child(2)').progressbar('value',ui.value)
        }
    });

}

$( function() {
    var handle = $( "#custom-handle" );
    $( "#slider" ).slider({
        orientation :'vertical',
        create: function() {
            handle.text( $( this ).slider( "value" ) );
        },
        slide: function( event, ui ) {
            handle.text( ui.value );
            $('body > div:nth-child(2)').progressbar('value',ui.value)
        }
    });
} );

$( function() {
    $("#tabs").tabs();
    $( "#accordion" ).accordion().draggable();
});

$( function() {
    $( "#accordion" ).each(function(i,elem){
        $(elem).attr('tittle',$(elem).text())
    });
});