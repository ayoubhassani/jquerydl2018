$('document').ready(JplaceHolder);
function JplaceHolder() {
    var ajaxSetting = {
        url: 'https://jsonplaceholder.typicode.com/users',
        complete: function (jqxhr, status) {
            if (status === "success") {
                var users = jqxhr.responseJSON;

                for (var i = 0; i < users.length; i++) {
                   var html =
                        '<table>' +
                            '<tr>'+
                                '<td >'+ users[i].name + '</td>'+
                                '<td >' + users[i].email + '</td>'+
                               '<td >' +users[i].address.suite + '</td>'+
                               '<td >' + users[i].address.street + '</td>'+
                               '<td >' + users[i].address.city + '</td>'+
                               '<td >' + users[i].address.zipcode + '</td>'+
                            '</tr>' +
                        '</table>';

                    $('body').append($(html))
                }
            }
        }
    }

    $.ajax(ajaxSetting);
}