$(document).ready(function () {

    $('body').css({
        height: '100%',
        margin: 0,
    });
    $('html').css({
        height: '100%',
        margin: 0,
    });
    $('body > div ').css({
        position: 'relative',
        height: '100%',
        width: '100%',
    }).children().css({
        position: 'absolute',
        //lageur total - lageur de l'element divisé par 2, même punition pour la hauteur
        // dans le soucis de le centrer exactement au centre de la page
        top: ($('body').height() - 100) / 2 + 'px',
        left: ($('body').width() - 100) / 2 + 'px',
        display: 'flex',
        justifyContent: "center",
        alignItems: 'center',
        width: "100px",
        height: '100px',
        backgroundColor: 'skyblue'
    }).children().css({
        width: "50px",
        height: '50px',
        backgroundColor: 'blue'

    });


    var etape = 1;
    setInterval(function () {

        var posActuel = $('body > div ').children().position();

        if (posActuel.top - 1 > 0 && posActuel.left - 1 > 0 && etape === 1) {

            //deplacement diagonal vers le haut guauche
            $('body > div ').children().css({
                top: posActuel.top - 1 + 'px',
                left: posActuel.left - 1 + 'px',
            })
            if (posActuel.top - 1 < 2){
                etape++;
                leftControl = posActuel.left - 1;
            }
        }
        if (etape === 2 && posActuel.left + 1 < $('body').width() - 101) {
            //doit se deplacer a droite

            $('body > div ').children().css({
                left: posActuel.left + 1 + 'px',
            })

            if (posActuel.left + 1 > $('body').width() - 102) etape++

        }
        if (posActuel.top + 1 < $('body').height() - 101 && etape === 3) {
            //doit se deplacer vers le bas

            $('body > div ').children().css({
                top: posActuel.top + 1 + 'px',
            })
            if(posActuel.top + 1 > $('body').height() - 102 )etape++


        }
        if (posActuel.left -1 > leftControl && etape === 4) {
            //doit se deplacer a guauche
            $('body > div ').children().css({
                left: posActuel.left -1 + 'px',
            })
            if(posActuel.left -1 < leftControl + 2) etape++
        }
        if (posActuel.top - 1 > ($('body').height() - 100) / 2  &&     etape === 5) {
            //retour au centre
            $('body > div ').children().css({
                top: posActuel.top - 1 + 'px',
                left: posActuel.left + 1 + 'px',
            })
        }


    }, 2);


});