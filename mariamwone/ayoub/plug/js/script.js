var ayoub = $;
((param)=> {
    param.fn.logo = function (settings) {
        var defaultsettings = {};
        var conf = $.extend(defaultsettings, settings);
        this.css({
            position: 'absolute',
            top: '10px',
            left: '10px'
        });
        return this;
    }
    param.fn.carrePeint = function (settings) {
        console.log(this);
        var defaultsettings = {
            c: "red",
            w: 10,
            h: 10
        };
        var conf = $.extend(defaultsettings, settings);
        this.css({
            height: conf.h + 'px',
            width: conf.w + 'px',
            backgroundColor: conf.c,
            border: '2px solid pink'
        });
        this.each((i, JsEleme)=> {
            jqEleme = $(JsEleme);
            if (i == 5) {
                jqEleme.html('position : 5')
            }
            if (i % 2 === 0) {
                jqEleme.css({
                    backgroundColor: 'blue',
                    border: '2px solid skyblue'
                })
            }
        })


        return this;
    }
    param.fn.accordeon = function () {

        // this la collection des element sur qui on applique le plugin
        console.log(this)
        this.find('div').hide();
        this.find('div.active').show()

        this.each(function (i, elem) {
            var me = $(elem);
            //alert( me.addClass("active").next().addClass("active").show());
            me.find('h3').click(function (event) {
                //
                me.find('div.active').hide().removeClass("active").prev().removeClass("active");
                $(event.target)
                    .addClass("active")
                    .next()
                    .addClass("active")
                    .show()
            });
        });

        return this;
    }
    param.fn.tabs = function () {
        this.find('.content > div').hide();
        var divsToShow = this.find('ul > li > a.active').attr('demba');
        $(divsToShow).show();
        var me = this;
        this.each(function(i,item){
            var me =$(item)
            me.find('ul > li > a').click((e)=> {
                e.preventDefault()
                var divsTohide = me.find('ul > li > a.active').attr('demba')
                me.find('ul > li > a.active').removeClass("active")
                $(divsTohide).hide();
                var divsToShow = $(e.target).attr('demba');
                $(e.target).addClass("active");
                $(divsToShow).show();

            })
        })

        return this;
    }
})(ayoub)


function createHtmlTabs() {

    $('body').append('<div class="tabs"></div>');
    $('.tabs').append('<div class="content"></div>')
    $('.content').append('<div id="div1">' +
        '000011111 Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias consectetur culpa, debitis dolores eos eveniet expedita explicabo itaque nam placeat porro quaerat quidem rem sed suscipit ullam ut. Cupiditate, pariatur.' +
        '</div>');
    $('.content').append('<div id="div2">' +
        '2 Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias consectetur culpa, debitis dolores eos eveniet expedita explicabo itaque nam placeat porro quaerat quidem rem sed suscipit ullam ut. Cupiditate, pariatur.' +
        '</div>');
    $('.content').append('<div id="div3">' +
        '3 Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias consectetur culpa, debitis dolores eos eveniet expedita explicabo itaque nam placeat porro quaerat quidem rem sed suscipit ullam ut. Cupiditate, pariatur.' +
        '</div>');
    $('.content').append('<div id="div4">' +
        '4 Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias consectetur culpa, debitis dolores eos eveniet expedita explicabo itaque nam placeat porro quaerat quidem rem sed suscipit ullam ut. Cupiditate, pariatur.' +
        '</div>');

    $('.tabs').prepend('<ul class="nav nav-tabs">' +
        '<li class="nav-item"><a demba="#div1" href="#" class="nav-link ">Tab 01</a></li>' +
        '<li class="nav-item"><a demba="#div2" class="nav-link active">Tab 02</a></li>' +
        '<li class="nav-item"><a demba="#div3" class="nav-link">Tab 03</a></li>' +
        '<li class="nav-item"><a demba="#div4" class="nav-link">Tab 04</a></li>' +
        '</ul>');


    $('.tabs').tabs()

}