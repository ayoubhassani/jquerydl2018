$('document').ready(ExoAccordeon);

function ExoAccordeon() {
    var f1 = function (event) {
        $(event.target).removeClass('active').next().hide(1000).removeClass('active')
    }
    var f2 = function (event) {
        //this represente l'element H2 sous sa forme javascript native
        //$(this) represente l'element H2 sous sa forme jQueryElement
        $('div.accordeon > h2.active').removeClass('active').next().hide()
        $(event.target).addClass('active').next().show(1000).addClass('active')
    }
    $('div.accordeon > div').hide()
    $('div.accordeon > div.active').show()
    $('div.accordeon > h2').click((event)=> {
        $(event.target).hasClass('active') ? f1(event) : f2(event);
    });
}