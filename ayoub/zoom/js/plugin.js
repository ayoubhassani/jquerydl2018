/**
 * Created by stagiaire on 23/01/2018.
 */
(function ($) {

    $.fn.ZommeMe = function () {
        //Ajout de css
        var styleObg = document.createElement('style')
        styleObg.textContent = '#zoom{width: 100px; height: 100px;position:absolute;border:1px solid red; border-radius:20px}'
        $('head').append(styleObg);

        console.log(this.attr('srcB'))
        this.mouseenter((e)=> {
            console.log(this)
            $('body').append('<div id="zoom"></div>');

        })
        this.mouseleave((e)=> {
            $('#zoom').remove()
        });

        this.each(function(i,item){
            var me =$(item)
            me.mousemove((e)=> {
                console.log($(window).scrollTop())
                var cliY = e.clientY - me.offset().top +$(window).scrollTop();
                var CliX = e.clientX - me.offset().left;

                // console.log("e.clientX : " + e.clientX, "e.clientY : " + e.clientY)
                // console.log("cliX : " + CliX, "cliY : " + cliY)

                var leftP = CliX * 100 / me.width()
                var topP = cliY * 100 / me.height()

                $('#zoom').css({
                    //positionde la div
                    top: (e.clientY + 10 + $(window).scrollTop()) + "px",
                    left: (e.clientX + 10) + "px",
                    //partie visible du background
                    backgroundImage: "url('"+me.attr('srcB')+"')",
                    backgroundPosition: leftP + '% ' + topP + '%',

                })
            });
        })

        return this;
    }
})($)


$(document).ready(function () {
    $('body').append('<img src="./img/viking_small.jpg" srcB="./img/viking_big.jpg">');
    $('body').append('<img src="./img/ciSmall.jpg" srcB="./img/ciBig.jpg">');
    $('img').ZommeMe();
});