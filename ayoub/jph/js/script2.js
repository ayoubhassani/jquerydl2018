$('document').ready(JplaceHolder);
function JplaceHolder() {

    $('#newUser').click(function(){
        window.location.href = '/jquery/ayoub/forms/'
    })
    var ajaxSetting = {
        url: 'https://jsonplaceholder.typicode.com/users',
        complete: function (jqxhr, status) {
            if (status === "success") {
                //data present
                //JSON.parse(jqxhr.responseText) equi jqxhr.responseJSON
                // jqxhr.responseJSON present seulement quand jquery et la reponse et json
                var users = jqxhr.responseJSON;

                for (var i = 0; i < users.length; i++) {
                    var html = '<div class="card" id="View'+users[i].id+'" >' +
                        '<span class="card-title">' + users[i].name + '</span>' +
                        '<div class="card-content" >' + users[i].email +
                        '<br/>' +
                        users[i].address.suite + ' ' +
                        users[i].address.street + ' ' +
                        users[i].address.city + ' ' +
                        users[i].address.zipcode + ' ' +
                        '</div>' +
                        '<div class="card-action"> ' +
                        '<a mody="' + users[i].id + '" class="showPosts" charge="false">Voir les posts</a>' +
                        '<a mody="' + users[i].id + '" class="deleteUsers" >Delete</a>' +
                        '<a mody="' + users[i].id + '" class="UpdateUser" >Update</a>' +
                        '</div>' +
                        '</div>';

                    $('body').append($(html))
                }
                $('.UpdateUser').click(function (event) {
                    //affiche et remple le formulaire
                    $(event.target).parent().parent().children().first().html('HASSANI AYOUB')
                });
                $('.deleteUsers').click(function (event) {
                    var id =$(event.target).attr('mody');
                    if(confirm("Etes vous sure de vouloir supprimer")){

                        $.ajax({
                            method:'DELETE',
                            url:"https://jsonplaceholder.typicode.com/posts/"+id,
                            complete:function(a,status){
                                if(status=="success"){
                                    console.log(a.responseJSON)
                                    $(event.target).parent().parent().remove()
                                    // $('#View'+id).remove()
                                }
                            }
                        })
                    }

                });

                // tous les buttons sont presents dans le body
                // je peu ajouter l'evenement
                $('.showPosts').click(function (event) {
                    event.preventDefault();
                    var id = parseInt($(event.target).attr('id'));
                    if(  $(event.target).attr('charge') ==='false' ) {
                        $.ajax({
                            url: 'https://jsonplaceholder.typicode.com/posts?userId=' + id,
                            complete: function (jqxhr, status) {
                                var userPosts = jqxhr.responseJSON;

                                var html = '<ul class="collection">';
                                for (var i = 0; i < userPosts.length; i++) {
                                    html += '<li class="collection-item">' + userPosts[i].title +
                                        '<div style="text-align: right">' +
                                        '<a id="' + userPosts[i].id + '" class="showComment btn-floating" >' +
                                        '<i id="' + userPosts[i].id + '" class="material-icons">edit</i>' +
                                        '</a>' +
                                        '</div>' +

                                        '</li>'
                                }
                                html += '</ul>';
                                ($(html)).insertAfter($(event.target).parent().parent());
                                $(event.target).attr('charge', 'true');
                                $('.showComment').click(function (event) {
                                    event.preventDefault();
                                    console.log(event.target)
                                    $.ajax({
                                        url: 'https://jsonplaceholder.typicode.com/comments?postId=' + event.target.id,
                                        complete: function (jqxhr, status) {
                                            var postComments = jqxhr.responseJSON;
                                            var html = '<ul class="collection">';
                                            for (var i = 0; i < postComments.length; i++) {
                                                html += '<li class="collection-item">' + postComments[i].body + '</li>'
                                            }
                                            html += '</ul>';
                                            ($(html)).insertAfter($(event.target).parent().parent())

                                        }
                                    });
                                });
                            }

                        });
                    }
                });


            }
        }
    };
    $.ajax(ajaxSetting)
}

