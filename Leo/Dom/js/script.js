$('document').ready(Ui);

  function Ui(){
    $('.one').append('<button>Inner</button>');
    ($('<button>InsertAfter</button>')).insertAfter($('.one'));
    ($('<button>InsertBefore</button>')).insertBefore($('.one'));

    // $('.two').append('<div>Une autre div</div>');
    
    // ----------------------------tabs--------------------------
    
    $('.three').html('<ul>'+
    '<li><a href="#tabs-1">Nunc tincidunt</a></li>'+
    '<li><a href="#tabs-2">Proin dolor</a></li>'+
    '<li><a href="#tabs-3">Aenean lacinia</a></li>'+
  '</ul>'+
  '<div id="tabs-1">'+
    '<p>Proin elit arcu, rutrum commodo, vehicula tempus, commodo a, risus. Curabitur nec arcu.</p>'+
  '</div>'+
  '<div id="tabs-2">'+
    '<p>Morbi tincidunt, dui sit amet facilisis feugiat, odio metus gravida ante, ut</p>'+
  '</div>'+
  '<div id="tabs-3">'+
    '<p>Mauris eleifend est et turpis. Duis id erat. Suspendisse potenti. Aliquam vulputate, pede vel </p>'+
    '<p>Duis cursus. Maecenas ligula eros, blandit nec, pharetra at, semper at, magna. Nullam ac </p>'+
  '</div>').tabs();



    ($('<h3>C\'est un petit titre</h3>')).insertBefore($('.two'));
    ($('<p>Suis dans P</p>')).insertAfter($('.two'));


    // ------------------------------Accordeon----------------------------

    $('.four').html('<h3>Section 1</h3>'+
  '<div>'+
    '<p>Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.</p>'+
  '</div>'+
  '<h3>Section 2</h3>'+
  '<div>'+
    '<p>Sed non urna. Donec et ante. Phasellus eu ligula. Vestibulum sit amet purus. Vivamus hendrerit, dolor at aliquet laoreet, mauris turpis porttitor velit, faucibus interdum tellus libero ac justo. Vivamus non quam. In suscipit faucibus urna.</p>'+
  '</div>'+
  '<h3>Section 3</h3>'+
  '<div>'+
    '<p>Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis. Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui.</p>'+
    '<ul>'+
      '<li>List item one</li>'+
      '<li>List item two</li>'+
      '<li>List item three</li>'+
    '</ul>'+
  '</div>'+
  '<h3>Section 4</h3>'+
  '<div>'+
    '<p>Cras dictum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean lacinia mauris vel est.</p>'+
    '<p>Suspendisse eu nisl. Nullam ut libero. Integer dignissim consequat lectus.Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>'+
  '</div>').accordion();


  }

  