$('document').ready(JplaceHolder);
function JplaceHolder() {
    $.ajax({
        url: "https://jsonplaceholder.typicode.com/users",
        complete: function (ajaxrequestObject, textStatus) {
            if (textStatus === "success") {
                usersList = ajaxrequestObject.responseJSON;
                for (var i = 0; i < usersList.length; i++) {
                    var html = '<div class="card">' +
                        '<span class="card-title">' + usersList[i].name + '</span>' +
                        '<div class="card-content" >' + usersList[i].email +
                        '<br/>' +
                        usersList[i].address.suite + ' ' +
                        usersList[i].address.street + ' ' +
                        usersList[i].address.city + ' ' +
                        usersList[i].address.zipcode + ' ' +
                        '</div>' +
                        '<div class="card-action"> ' +
                        '<a id="'+usersList[i].id+'" class="showPosts" ayoub="'+usersList[i].id+'">Voir les posts</a>' +
                        '</div>' +
                        '</div>';
                    $('body').append($(html));

                }
                $('.showPosts').click(function(event){
                    $.ajax({
                        url: "https://jsonplaceholder.typicode.com/posts?userId="+$(event.target).attr('ayoub'),
                        complete: function (ajaxrequestObject, textStatus) {
                          var userPosts = ajaxrequestObject.responseJSON;
                            var html = '<ul class="collection">';

                            for (var i = 0; i < userPosts.length; i++) {
                               html+= '<li class="collection-item">' + userPosts[i].title + '</li>'
                            }

                            html +='</ul>';
                                ($( html)).insertAfter($(".showPosts"));
                        }});

                });
            }
        }
    });
}

