$('document').ready(JplaceHolder);
function JplaceHolder() {
    var ajaxSetting = {
        url: 'https://jsonplaceholder.typicode.com/users',
        complete: function (jqxhr, status) {
            if (status === "success") {
                //data present
                //JSON.parse(jqxhr.responseText) equi jqxhr.responseJSON
                // jqxhr.responseJSON present seulement quand la réponse de jquery est json
                var users = jqxhr.responseJSON;

                for (var i = 0; i < users.length; i++) {
                    var html = '<div class="card" id="View'+users[i].id+'">' +
                        '<span class="card-title">' + users[i].name + '</span>' +
                        '<div class="card-content" >' + users[i].email +
                        '<br/>' +
                        users[i].address.suite + ' ' +
                        users[i].address.street + ' ' +
                        users[i].address.city + ' ' +
                        users[i].address.zipcode + ' ' +
                        '</div>' +
                        '<div class="card-action"> ' +
                        '<a id="' + users[i].id + 'show" class="showPosts" ayoub="' + users[i].id + '">Voir les posts</a>' +
                        '<a id="' + users[i].id + 'delete" class="deleteUser" ayoub="' + users[i].id + '">delete</a>' +
                        '<a id="' + users[i].id + 'update" class="updateUser" ayoub="' + users[i].id + '">Maj</a>' +
                        '</div>' +
                        '</div>';

                    $('body').append($(html))
                }
                // tous les buttons sont presents dans le body
                // je peu ajouter l'evenement
                $('.showPosts').click(function (event) {
                    // href : pour éviter le rechargement de la page
                    event.preventDefault();
                    $.ajax({
                        // Pour récupérer la valeur bidon : event.target.id fonctionne car id est un elt natif HTML de la balise <a id="selection"> contrairement à <a bidon="selection"> où il faudra saisir $(event.target).attr("bidon")
                        url: 'https://jsonplaceholder.typicode.com/posts?userId=' + event.target.id,
                        complete: function (jqxhr, status) {
                            var userPosts = jqxhr.responseJSON;

                            var html = '<ul class="collection">';
                            for (var i = 0; i < userPosts.length; i++) {
   //                             html += '<li class="collection-item">' + userPosts[i].body + '<a pstId="' + userPosts[i].id + '" class="showComments">lien</a></li>'
                                html += '<li class="collection-item">' + userPosts[i].body
                                html +='<div style="text-align:right"><a pstId="' + userPosts[i].id + '" class="btn-floating btn-small waves-effect waves-light green showComments "><i pstId="' + userPosts[i].id + '" class="material-icons">+</i></a></div></li>'

                            }
                            html += '</ul>';
                            ($(html)).insertAfter($(event.target).parent().parent())
                            $('.showComments').click(function (event) {
                                    $.ajax({
                                        //url: 'https://jsonplaceholder.typicode.com/comments/?postId=' + $(event.target).attr('pstid'),
                                        url: 'https://jsonplaceholder.typicode.com/comments/?postId=' + $(event.target).attr('pstId'),
                                        complete: function (jqxhr2, status2) {
                                            var userPostsComm = jqxhr2.responseJSON;
console.log(userPostsComm)
                                            var html = '<ul class="collection">';
                                            for (var i = 0; i < userPostsComm.length; i++) {
                                                html += '<li class="collection-item"> De ' + userPostsComm[i].email + ' ' + userPostsComm[i].body + '</li>'
                                            }
                                            html +='</ul>';

                                            ($(html)).insertAfter($(event.target).parent().parent())
                                        }

                                    })
                                }
                            )
                        }
                    });


                })
                $('.deleteUser').click(function (event) {
                    // href : pour éviter le rechargement de la page
                   // event.preventDefault();
                    var numView=parseInt(event.target.id)
                    $.ajax({
                        // Pour récupérer la valeur bidon : event.target.id fonctionne car id est un elt natif HTML de la balise <a id="selection"> contrairement à <a bidon="selection"> où il faudra saisir $(event.target).attr("bidon")
                        method:'DELETE',
                        url: 'https://jsonplaceholder.typicode.com/users/' + event.target.id,
                        complete: function (jqxhr, status) {
                            console.log('delete')
                            console.log(jqxhr.responseJSON)
                    //        $("#View"+event.target.id).remove()
                            $("#View"+numView).remove()
                        }
                    });


                })
            }
        }
    }
    $.ajax(ajaxSetting)
}

