$('document').ready(APIRest);
function  APIRest() {
    $.ajax({
        method:'GET',
        url:"https://jsonplaceholder.typicode.com/posts/1",
        complete:function (xhr,status) {
            if(status=="success"){
                console.log(xhr.responseJSON)

            }
            
        }
    })
    function Ajout() {
        var d = {
            title: "Titre",
            body: "test body..................../"
        }

        $.ajax({
            method: 'POST',
            contentType: "application/json",
            data: JSON.stringify(d),
            url: "https://jsonplaceholder.typicode.com/posts",
            complete: function (xhr, status) {
                if (status == "success") {
                    console.log(xhr.responseJSON)

                }
            }
        })
    }
    function Maj() {
        var id = prompt('id');
        var d = {
            title: prompt('title'),
        }

        $.ajax({
            method: 'PUT',
            contentType: "application/json",
            data: JSON.stringify(d),
            url: "https://jsonplaceholder.typicode.com/posts/" + id,
            complete: function (xhr, status) {
                if (status == "success") {
                    console.log(xhr.responseJSON)

                }
            }
        })
    }
        function Sup() {
            var id=prompt('id');
            $.ajax({
                method: 'DELETE',
                url: "https://jsonplaceholder.typicode.com/posts/"+id,
                complete: function (xhr, status) {
                    if (status == "success") {
                        console.log(xhr.responseJSON)

                    }
                }
            })
    }

    $('body').append('<button id="boutonCrea">click</button>');
    $('body').append('<button id="boutonMaj">Maj</button>');
    $('body').append('<button id="boutonSup">Delete</button>');
    $('#boutonCrea').click(Ajout);
    $('#boutonMaj').click(Maj);
    $('#boutonSup').click(Sup);
    }