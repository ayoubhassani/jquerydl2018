$('document').ready(Events);
function Events() {
    /**
     * click
     * dblClick
     * mouspressed
     * mouseup
     * mousedown
     *
     * mousenter
     * mouseout
     *
     * mouseover
     * mouslever
     *
     */

    var div = $('div').first()
    var innerdiv = $('div').first().children().first()
    innerdiv.css({
        width: '50px',
        height: '50px',
        backgroundColor: 'red'
    })
    console.log(innerdiv)
    div.css({
        width: '100px',
        height: '100px',
        backgroundColor: 'skyblue'
    })


    div.click(function (e) {
        console.log('click',e)
    });
    div.mousedown(function (e) {
        console.log('mousedown',e)
    });
    div.mouseup(function (e) {
        console.log('mouseup',e)
    });
    div.contextmenu(function (e) {
        console.log('contextmenu',e)
    });

    div.dblclick(function (e) {
        console.log('dblClick',e)
    })
    innerdiv.mouseenter(function (e) {
        console.log('innerdiv mouseenter',e)
    })
    div.mouseenter(function (e) {
        console.log('mouseenter',e)
    })
    div.mouseleave(function (e) {
        console.log('mouseleave',e)
    })

    div.mousemove(function (e) {
        console.log('mousemove',e)
    })

    div.mouseover(function (e) {
        console.log('mouseover',e)
    })
    div.mouseout(function (e) {
        console.log('mouseout',e)
    })

var textField = $('input').first()



    textField.keydown(function (e) {
        console.log('keydown',e)
    })
    textField.keyup(function (e) {
        console.log('keyup',e)
    })
    textField.keypress(function (e) {
        console.log('keypress',e)
        if(e.key==="Enter"&& !e.ctrlKey )console.log('debut de la recherche')
        //ctrl + Enter est le retour a la ligne par default
        if(e.keyCode===10)console.log('debut de la recherche personnalisé')

    })

    textField.focus(function (e) {
        console.log('focus',e)
    })
    textField.blur(function (e) {
        console.log('blur',e)
    })
}
